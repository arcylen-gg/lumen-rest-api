<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    // REGISTER A USER
    $router->post('register', 'UserController@registerUser');

    // LIST OF TODO
    $router->get('todo-list', 'TodoController@showList');

    // AUTH
    $router->group(['middleware' => 'auth'], function () use ($router) {
        // CREATE TODO
        $router->post('todo-list', 'TodoController@createTodo');
        // UPDATE TODO
        $router->patch('todo-list/{id}', 'TodoController@updateTodo');
        // DELETE TODO
        $router->delete('todo-list/{id}', 'TodoController@deleteTodo');
    });
});