<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repository\TodoRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    private $todo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TodoRepositoryInterface $todo)
    {
        $this->todo = $todo;
    }

    //
    public function showList()
    {
        return response()->json(['list' => $this->todo->showList()]);
    }

    public function createTodo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'unique:todos',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'The given data is invalid',
                'error' => $validator->messages()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $isSaved = $this->todo->createTodo($request->all());
        return response()->json(
            ['data' => $isSaved],
            $isSaved === true ? Response::HTTP_CREATED : Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function updateTodo(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'unique:todos',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'The given data is invalid',
                'error' => $validator->messages()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $isSaved = $this->todo->updateTodo($request->all(), $id);
        return response()->json(
            ['data' => $isSaved],
            $isSaved === true ? Response::HTTP_ACCEPTED : Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function deleteTodo(int $id)
    {
        $isDeleted = $this->todo->deleteTodo($id);

        if ($isDeleted) {
            return response()->json(['data', true], Response::HTTP_ACCEPTED);
        } 

        throw new \Exception('Cannot delete entity', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
