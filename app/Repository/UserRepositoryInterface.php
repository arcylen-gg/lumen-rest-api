<?php
declare(strit_types=1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface UserRepository
 * @package App\Repository;
 */
interface UserRepositoryInterface
{
    /**
     * @param array $attributes
     * 
     * @return Bool
     */
    public function createUser(array $attributes): Bool;
}