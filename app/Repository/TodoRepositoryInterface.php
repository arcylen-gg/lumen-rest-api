<?php
declare(strit_types=1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface TodoRepository
 * @package App\Repository;
 */
interface TodoRepositoryInterface
{
    /**
     * @return array
    */
    public function showList(): ?Array;

    /**
     * @param array $attributes
     * 
     * @return Bool
    */
    public function createTodo(array $attributes): Bool;

    /**
     * @param array $attributes
     * 
     * @return Bool
    */
    public function updateTodo(array $attributes, int $id): Bool;

    /**
     * @param int $id
     * 
     * @return Bool
    */
    public function deleteTodo(int $id): Bool;
}