<?php
declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Repository\TodoRepositoryInterface;
use App\Repository\Eloquent\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Todo;

class TodoRepository extends BaseRepository implements TodoRepositoryInterface
{
    /**
     * @param Todo
     */

    public function __construct(Todo $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Array
     */
    public function showList(): ?Array
    {
        return $this->model::all()->toArray();
    }

    /**
     * @param array $attributes
     * 
     * @return Bool
     */
    public function createTodo(array $attributes): Bool
    {
        return $this->model->create($attributes) ? true : false;
    }

    /**
     * @param array $attributes
     * 
     * @return Bool
     */
    public function updateTodo(array $attributes, int $id): Bool
    {
        $update = $this->model->find($id);
        $update->title = $attributes['title'];
        $update->content = $attributes['content'];
        $update->tag = $attributes['tag'];
        return $update->save() ? true : false;
    }

    /**
     * @param int $id
     * 
     * @return Bool
     */
    public function deleteTodo(int $id): Bool
    {
        $delete = $this->model->find($id);
        return $delete->delete();
    }
}