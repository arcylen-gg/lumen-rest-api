<?php
declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Repository\UserRepositoryInterface;
use App\Repository\Eloquent\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @param User
     */

    public function __construct(User $model)
    {
        parent::__construct($model);
    }
    
    public function createUser(array $attributes): Bool
    {
        $attributes['api_token'] = Str::random(60);
        return $this->model->create($attributes) ? true : false;
    }
}