<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\{ EloquentRepositoryInterface,
    TodoRepositoryInterface,
    UserRepositoryInterface
};
use App\Repository\Eloquent\{ BaseRepository,
    TodoRepository,
    UserRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(TodoRepositoryInterface::class, TodoRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}